# This file is the entry point to configure your own services.
# Files in the packages/ subdirectory configure your dependencies.

# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: 'fr'

imports:
    - { resource: images.yaml }
    - { resource: mass.yaml }

services:
    # default configuration for services in *this* file
    _defaults:
        autowire: true      # Automatically injects dependencies in your services.
        autoconfigure: true # Automatically registers your services as commands, event subscribers, etc.
        public: false       # Allows optimizing the container by removing unused services; this also means
                            # fetching services directly from the container via $container->get() won't work.
                            # The best practice is to be explicit about your dependencies anyway.

    # cache
    app.simple_cache:
        class: Symfony\Component\Cache\Simple\Psr6Cache
        arguments: ['@app.cache.mobicoop']

    # makes classes in src/ available to be used as services
    # this creates a service per class whose id is the fully-qualified class name
    App\:
        resource: '../src/*'
        exclude: '../src/{DependencyInjection,Entity,Migrations,Tests,Kernel.php}'

    # controllers are imported separately to make sure services can be injected
    # as action arguments even if you don't extend any base controller class
    App\Controller\:
        resource: '../src/Controller'
        tags: ['controller.service_arguments']


    # EVENT LISTENERS

    'App\EventListener\DeserializeListener':
        tags:
            - { name: 'kernel.event_listener', event: 'kernel.request', method: 'onKernelRequest', priority: 2 }
        # Autoconfiguration must be disabled to set a custom priority
        autoconfigure: false
        decorates: 'api_platform.listener.request.deserialize'
        arguments:
            $decorated: '@App\EventListener\DeserializeListener.inner'

    # JWT Event Listener
    mobicoop.event.jwt_created_listener:
        class: App\Security\EventListener\JWTCreatedListener
        tags:
            - { name: kernel.event_listener, event: lexik_jwt_authentication.on_jwt_created, method: onJWTCreated }
    
    # Direction Load Listener
    mobicoop.direction.direction_load_listener:
        class: App\Geography\EventListener\DirectionLoadListener
        arguments: ['@App\Geography\Service\GeoTools'] # GeoTools injected to compute CO2
        tags:
            - { name: doctrine.event_listener, event: postLoad }

    # Address Load Listener
    mobicoop.address.address_load_listener:
        class: App\Geography\EventListener\AddressLoadListener
        arguments: ['@App\Geography\Service\GeoTools'] # GeoTools injected for displayLabel
        tags:
            - { name: doctrine.event_listener, event: postLoad }

    # Community User Load Listener
    mobicoop.community_user.community_user_load_listener:
        class: App\Community\EventListener\CommunityUserLoadListener
        arguments: ['@request_stack']
        tags:
            - { name: doctrine.event_listener, event: postLoad }

    # Community Load Listener
    mobicoop.community.community_load_listener:
        class: App\Community\EventListener\CommunityLoadListener
        arguments: ['@request_stack']
        tags:
            - { name: doctrine.event_listener, event: postLoad }
    
    # User Load Listener
    mobicoop.user.user_load_listener:
        class: App\User\EventListener\UserLoadListener
        arguments:
            $params:
                avatarSizes: '%env(string:AVATAR_SIZES)%'
                avatarDefaultFolder: '%env(string:AVATAR_DEFAULT_FOLDER)%'
        tags:
            - { name: doctrine.event_listener, event: postLoad }
        

    # CONTROLLERS AND SERVICES

    # Article
    App\Article\Controller\:
        resource: '../src/Article/Controller'
        tags: ['controller.service_arguments']

    # Auth
    App\Auth\Controller\:
        resource: '../src/Auth/Controller'
        tags: ['controller.service_arguments']

    # Carpool
    App\Carpool\Controller\:
        resource: '../src/Carpool/Controller'
        tags: ['controller.service_arguments']

    'App\Carpool\Filter\LocalityFilter':
        # Uncomment only if autoconfiguration isn't enabled
        #tags: [ 'api_platform.filter' ]

    App\Carpool\Service\AskManager:
        tags:
            - { name: monolog.logger, channel: carpool }

    App\Carpool\Service\AdManager:
        arguments:
            $params:
                defaultMarginDuration: '%env(int:CARPOOL_MARGIN_DURATION)%'
                defaultSeatsDriver: '%env(int:CARPOOL_DEFAULT_SEATS_DRIVER)%'
                defaultSeatsPassenger: '%env(int:CARPOOL_DEFAULT_SEATS_PASSENGER)%'
                carpoolProofDistance: '%env(int:CARPOOL_PROOF_DISTANCE)%'
                proofType: '%env(string:CARPOOL_PROOF_DYNAMIC_TYPE)%'
        tags:
            - { name: monolog.logger, channel: carpool }

    App\Carpool\Service\ProofManager:
        arguments:
            $provider: '%env(string:CARPOOL_PROOF_PROVIDER)%'
            $uri: '%env(string:CARPOOL_PROOF_URI)%'
            $token: '%env(string:CARPOOL_PROOF_TOKEN)%'
            $proofType: '%env(string:CARPOOL_PROOF_CLASSIC_TYPE)%'
        tags:
            - { name: monolog.logger, channel: carpool_proof }

    App\Carpool\Service\DynamicManager:
        arguments:
            $params:
                defaultSeatsDriver: '%env(int:CARPOOL_DEFAULT_SEATS_DRIVER)%'
                defaultSeatsPassenger: '%env(int:CARPOOL_DEFAULT_SEATS_PASSENGER)%'
                dynamicProofDistance: '%env(int:DYNAMIC_CARPOOL_PROOF_DISTANCE)%'
                dynamicReachedDistance: '%env(int:DYNAMIC_CARPOOL_REACHED_DISTANCE)%'
                dynamicDestinationDistance: '%env(int:DYNAMIC_CARPOOL_DESTINATION_DISTANCE)%'
                dynamicMaxSpeed: '%env(int:DYNAMIC_CARPOOL_MAX_SPEED)%'
                dynamicEnableMaxSpeed: '%env(bool:DYNAMIC_CARPOOL_ENABLE_MAX_SPEED)%'
                proofType: '%env(string:CARPOOL_PROOF_DYNAMIC_TYPE)%'
        tags:
            - { name: monolog.logger, channel: carpool_dynamic }

    App\Carpool\Service\ProposalManager:
        arguments:
            $params:
                defaultRole: '%env(int:CARPOOL_ROLE)%'
                defaultType: '%env(int:CARPOOL_TYPE)%'
                defaultUseTime: '%env(bool:CARPOOL_USE_TIME)%'
                defaultStrictDate: '%env(bool:CARPOOL_STRICT_DATE)%'
                defaultStrictPunctual: '%env(bool:CARPOOL_STRICT_PUNCTUAL)%'
                defaultStrictRegular: '%env(bool:CARPOOL_STRICT_REGULAR)%'
                defaultMarginDuration: '%env(int:CARPOOL_MARGIN_DURATION)%'
                defaultRegularLifeTime: '%env(int:CARPOOL_REGULAR_LIFETIME)%'
                defaultAnyRouteAsPassenger: '%env(bool:CARPOOL_ANY_ROUTE_PASSENGER)%'
                defaultPriceKm: '%env(float:CARPOOL_PRICE)%'
        tags:
            - { name: monolog.logger, channel: carpool }
    
    App\Carpool\Service\ProposalMatcher:
        arguments:
            $params:
                dynamicMaxPendingTime: '%env(int:DYNAMIC_CARPOOL_MAX_PENDING_TIME)%'
        tags:
            - { name: monolog.logger, channel: carpool }

    # Communication
    App\Communication\Controller\:
        resource: '../src/Communication/Controller'
        arguments:
                $params:
                    smsUsername: '%env(string:SMS_USERNAME)%'
                    smsPassword: '%env(string:SMS_PASSWORD)%'
                    smsSender: '%env(string:SMS_SENDER)%'
        tags: ['controller.service_arguments']

    App\Communication\Service\NotificationManager:
        arguments:
            $emailTemplatePath: '%env(resolve:NOTIFICATION_TEMPLATE_EMAIL_PATH)%'
            $emailTitleTemplatePath: '%env(resolve:COMMUNICATION_TEMPLATE_EMAIL_PATH)%%env(resolve:NOTIFICATION_TITLE_TEMPLATE_EMAIL_PATH)%'
            $pushTemplatePath: '%env(resolve:NOTIFICATION_TEMPLATE_PUSH_PATH)%'
            $pushTitleTemplatePath: '%env(resolve:COMMUNICATION_TEMPLATE_PUSH_PATH)%%env(resolve:NOTIFICATION_TITLE_TEMPLATE_PUSH_PATH)%'
            $smsTemplatePath: '%env(resolve:NOTIFICATION_TEMPLATE_SMS_PATH)%'
            $enabled: '%env(bool:resolve:NOTIFICATION_ENABLED)%'
        tags:
            - { name: monolog.logger, channel: notification }

    App\Communication\Service\EmailManager:
        arguments:
            $emailSender: '%env(resolve:MAILER_SENDER)%'
            $emailSenderName: '%env(resolve:MAILER_SENDER_NAME)%'
            $emailReplyTo: '%env(resolve:MAILER_REPLYTO)%'
            $emailReplyToName: '%env(resolve:MAILER_REPLYTO_NAME)%'
            $templatePath: '%env(resolve:COMMUNICATION_TEMPLATE_EMAIL_PATH)%'
            $emailAdditionalHeaders: '%env(resolve:MAILER_ADDITIONAL_HEADERS)%'
            $emailSupport: '%env(resolve:MAILER_SUPPORT)%'
        tags:
            - { name: monolog.logger, channel: communication }

    App\Communication\Service\InternalMessageManager:
        tags:
            - { name: monolog.logger, channel: communication }

    App\Communication\Service\PushManager:
        arguments:
            $templatePath: '%env(resolve:COMMUNICATION_TEMPLATE_PUSH_PATH)%'
            $pushProvider: '%env(resolve:PUSH_PROVIDER)%'
            $apiToken: '%env(resolve:PUSH_API_TOKEN)%'
            $senderId: '%env(resolve:PUSH_SENDER_ID)%'
        tags:
            - { name: monolog.logger, channel: communication }

    App\Communication\Service\SmsManager:
        arguments:
            $templatePath: '%env(resolve:COMMUNICATION_TEMPLATE_SMS_PATH)%'
            $smsProvider: '%env(resolve:SMS_PROVIDER)%'
            $username: '%env(resolve:SMS_USERNAME)%'
            $password: '%env(resolve:SMS_PASSWORD)%'
            $sender: '%env(resolve:SMS_SENDER)%'
        tags:
            - { name: monolog.logger, channel: communication }
    
    App\Communication\EntityListener\RecipientListener:
        tags:
            - { name: doctrine.orm.entity_listener, lazy: true }    # lazy set to true to avoid doctrine UnitOfWork error on insert

    App\Communication\EventSubscriber\ContactSubscriber:
        arguments:
            $emailTemplatePath: '%env(resolve:NOTIFICATION_TEMPLATE_EMAIL_PATH)%'
            $contactEmailAddress: '%env(resolve:MAILER_CONTACT)%'
            $contactEmailObject: '%env(resolve:MAILER_CONTACT_OBJECT)%'
            $supportEmailAddress: '%env(resolve:MAILER_SUPPORT)%'
            $supportEmailObject: '%env(resolve:MAILER_SUPPORT_OBJECT)%'

    # Community
    App\Community\Controller\:
        resource: '../src/Community/Controller'
        tags: ['controller.service_arguments']

    App\Community\Service\CommunityManager:
        arguments:
            $securityPath: '%env(resolve:COMMUNITY_SECURITY_PATH)%'
        tags:
            - { name: monolog.logger, channel: community }

    # Event
    App\Event\Controller\:
        resource: '../src/Event/Controller'
        tags: ['controller.service_arguments']

    App\Event\Controller\ReportAction:
        arguments:
            $supportEmail: '%env(resolve:MAILER_SUPPORT)%'
            $senderEmail: '%env(resolve:MAILER_SENDER)%'
            $emailTemplatePath: '%env(resolve:NOTIFICATION_TEMPLATE_EMAIL_PATH)%'
        tags: ['controller.service_arguments']

    # External Journey
    App\ExternalJourney\Service\ExternalJourneyManager:
        arguments:
            $operator: '%env(json:file:resolve:RDEX_OPERATOR)%'
            $clients: '%env(json:file:resolve:RDEX_CLIENTS)%'
            $providers: '%env(json:file:resolve:RDEX_PROVIDERS)%'

    # Geocoder; alias to allow this type to be autowired
    Geocoder\Plugin\PluginProvider: '@bazinga_geocoder.provider.chain'

    # Georouter, add prioritization
    Georouter.query_data_plugin:
        class: Geocoder\Plugin\Plugin\QueryDataPlugin
        arguments:
            - '%env(json:SIG_GEOCODER_PRIORITIZE_COORDINATES)%'
    # Geography
    App\Geography\Controller\:
        resource: '../src/Geography/Controller'
        tags: ['controller.service_arguments']

    App\Geography\EventSubscriber\CarpoolSubscriber:
        arguments:
            $directory: '%env(resolve:ASYNC_GEO_TEMP)%'

    App\Geography\Service\AddressManager:
        tags:
            - { name: monolog.logger, channel: geography }

    App\Geography\Service\GeoRouter:
        arguments:
            $uri: '%env(resolve:SIG_GEOROUTER_URI)%'
            $type: '%env(resolve:SIG_GEOROUTER_TYPE)%'
            $batchScriptPath: '%env(resolve:SIG_GEOROUTER_BATCH_SCRIPT_PATH)%'
            $batchScriptArgs: '%env(resolve:SIG_GEOROUTER_BATCH_SCRIPT_ARGS)%'
            $batchTemp: '%env(resolve:SIG_GEOROUTER_BATCH_TEMP)%'
        tags:
            - { name: monolog.logger, channel: georouter }

    App\Geography\Service\GeoTools:
        arguments:
            $params:
                displayCountry: '%env(resolve:DISPLAY_COUNTRY)%'
                displayRegion: '%env(resolve:DISPLAY_REGION)%'
                displaySubRegion: '%env(resolve:DISPLAY_SUBREGION)%'
                displayLocality: '%env(resolve:DISPLAY_LOCALITY)%'
                displayPostalCode: '%env(resolve:DISPLAY_POSTALCODE)%'
                displayStreetAddress: '%env(resolve:DISPLAY_STREETADDRESS)%'
                displayVenue: '%env(resolve:DISPLAY_VENUE)%'
                displayRelayPoint: '%env(resolve:DISPLAY_RELAY_POINT)%'
                displayNamed: '%env(resolve:DISPLAY_NAMED)%'
                displayEvent: '%env(resolve:DISPLAY_EVENT)%'
                displaySeparator: '%env(resolve:DISPLAY_SEPARATOR)%'
    
    App\Geography\Service\GeoSearcher:
        arguments:
            $iconPath: '%env(resolve:GEOCOMPLETE_ICONS_PATH)%'
            $dataPath: '%env(resolve:DATA_URI)%'
            $defaultSigResultNumber: '%env(resolve:GEOCOMPLETE_SIG_DEFAULT_RESULTS)%'
            $defaultNamedResultNumber: '%env(resolve:GEOCOMPLETE_NAMED_DEFAULT_RESULTS)%'
            $defaultRelayPointResultNumber: '%env(resolve:GEOCOMPLETE_RELAY_POINTS_DEFAULT_RESULTS)%'
            $defaultEventResultNumber: '%env(resolve:GEOCOMPLETE_EVENTS_DEFAULT_RESULTS)%'
            $geoDataFixes: '%env(json:file:resolve:SIG_GEOCODER_FIXER_DATA)%'
            $distanceOrder: '%env(bool:SIG_GEOCODER_PRIORITIZE_ORDER)%'

    App\Geography\Service\TerritoryManager:
        tags:
            - { name: monolog.logger, channel: geography }

    # Image
    App\Image\Controller\:
        resource: '../src/Image/Controller'
        tags: ['controller.service_arguments']

    App\Image\EntityListener\ImageListener:
        arguments: ['@App\Image\Service\ImageManager']
        tags:
            - { name: doctrine.orm.entity_listener, lazy: true }    # lazy set to true to avoid doctrine UnitOfWork error on insert

    App\Image\Service\ImageManager:
        arguments:
            $types: '%images%'

    # Import
    App\Import\Controller\:
        resource: '../src/Import/Controller'
        tags: ['controller.service_arguments']
    
    # Mass Communication
    App\MassCommunication\Controller\:
        resource: '../src/MassCommunication/Controller'
        tags: ['controller.service_arguments']

    App\MassCommunication\Service\CampaignManager:
        arguments:
            $mailerProvider: '%env(resolve:MASS_MAILER_PROVIDER)%'
            $mailerApiUrl: '%env(resolve:MASS_MAILER_API_URL)%'
            $smsProvider: '%env(resolve:MASS_SMS_PROVIDER)%'
            $mailerApiKey: '%env(resolve:MASS_MAILER_API_KEY)%'
            $mailTemplate: '%env(resolve:MASS_MAILER_BASE_TEMPLATE)%'

    # Match
    App\Match\Controller\:
        resource: '../src/Match/Controller'
        tags: ['controller.service_arguments']

    App\Match\Service\GeoMatcher:
        tags:
            - { name: monolog.logger, channel: geomatcher }

    App\Match\Service\MassImportManager:
        arguments:
            $params: '%mass%'
            $emailTemplatePath: '%env(resolve:MOBIMATCH_EMAIL_TEMPLATE_PATH)%'
        tags:
            - { name: monolog.logger, channel: mass }

    App\Match\Service\MassMigrateManager:
        arguments:
            $params:
                chat: '%env(int:CARPOOL_DEFAULT_CHAT)%'
                music: '%env(int:CARPOOL_DEFAULT_MUSIC)%'
                smoke: '%env(bool:CARPOOL_DEFAULT_SMOKE)%'
        tags:
            - { name: monolog.logger, channel: mass }

    App\Match\Service\MassPublicTransportPotentialManager:
        arguments:
            $params:
                ptProvider: '%env(string:MOBIMATCH_PT_PROVIDER)%'
                ptAlgorithm: '%env(string:MOBIMATCH_PT_ALGORITHM)%'
                ptMaxConnections: '%env(int:MOBIMATCH_PT_MAXIMAL_CONNECTIONS)%'
                ptMaxDistanceWalkFromHome: '%env(int:MOBIMATCH_PT_MAXIMUM_DISTANCE_WALK_FROM_HOME)%'
                ptMaxDistanceWalkFromWork: '%env(int:MOBIMATCH_PT_MAXIMUM_DISTANCE_WALK_FROM_WORK)%'
        tags:
            - { name: monolog.logger, channel: mass }

    # Price
    App\Price\Controller\:
        resource: '../src/Price/Controller'
        tags: ['controller.service_arguments']

    # Rdex
    App\Rdex\Controller\:
        resource: '../src/Rdex/Controller'
        tags: ['controller.service_arguments']
    
    # User
    App\User\Controller\:
        resource: '../src/User/Controller'
        tags: ['controller.service_arguments']

    'App\User\DataProvider\UserSearchCollectionDataProvider':
        arguments:
            $collectionExtensions: !tagged api_platform.doctrine.orm.query_extension.collection

    'App\User\Filter\HomeAddressTerritoryFilter':
        # Uncomment only if autoconfiguration isn't enabled
        #tags: [ 'api_platform.filter' ]

    App\User\Service\UserManager:
        arguments:
            $chat: '%env(int:CARPOOL_DEFAULT_CHAT)%'
            $music: '%env(int:CARPOOL_DEFAULT_MUSIC)%'
            $smoke: '%env(bool:CARPOOL_DEFAULT_SMOKE)%'
            $fakeFirstMail: '%env(string:FAKE_FIRST_MAIL)%'
            $fakeFirstToken: '%env(string:FAKE_FIRST_TOKEN)%'
    
    App\ExternalJourney\DataProvider\ExternalJourneyCollectionDataProvider:
        arguments:
            $params:
                avatarSizes: '%env(string:AVATAR_SIZES)%'
                avatarDefaultFolder: '%env(string:AVATAR_DEFAULT_FOLDER)%'

    # Solidary
    App\Solidary\Controller\:
        resource: '../src/Solidary/Controller'
        tags: ['controller.service_arguments']

    App\Solidary\Service\SolidaryMatcher:
        arguments:
            $params:
                solidaryMMinRangeTime: '%env(string:SOLIDARY_M_MIN_RANGE_TIME)%'
                solidaryMMaxRangeTime: '%env(string:SOLIDARY_M_MAX_RANGE_TIME)%'
                solidaryAMinRangeTime: '%env(string:SOLIDARY_A_MIN_RANGE_TIME)%'
                solidaryAMaxRangeTime: '%env(string:SOLIDARY_A_MAX_RANGE_TIME)%'
                solidaryEMinRangeTime: '%env(string:SOLIDARY_E_MIN_RANGE_TIME)%'
                solidaryEMaxRangeTime: '%env(string:SOLIDARY_E_MAX_RANGE_TIME)%'

    App\Solidary\Service\SolidaryContactManager:
        arguments:
            $notificationsEnabled: '%env(bool:resolve:NOTIFICATION_ENABLED)%'

    App\Solidary\Service\SolidaryUserManager:
        arguments:
            $params:
                chat: '%env(int:CARPOOL_DEFAULT_CHAT)%'
                music: '%env(int:CARPOOL_DEFAULT_MUSIC)%'
                smoke: '%env(bool:CARPOOL_DEFAULT_SMOKE)%'            

    # Utility
    App\Utility\Service\VersionManager:
        arguments:
            $repositoryFile: '%env(string:MOBILE_JSON_REPOSITORY)%'
            $appId: '%env(string:MOBILE_APP_ID)%'