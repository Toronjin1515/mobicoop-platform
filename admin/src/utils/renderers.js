import React from 'react';

export const addressRenderer = (address) =>
  address && address.displayLabel && address.displayLabel.length
    ? `${address.displayLabel[0]} - ${address.displayLabel[1]}`
    : '';

export const usernameRenderer = ({ record }) => `${record.givenName} ${record.familyName}`;

export const UserRenderer = ({ record }) => <span>{usernameRenderer({ record })} </span>;

export const solidaryLabelRenderer = ({ record }) =>
  record.originId
    ? `#${record.originId} - ${record.displayLabel} ${
        record.solidaryUser
          ? `/ ${usernameRenderer({
              record: record.solidaryUser.user,
            })}`
          : ''
      }`
    : '';
