const SOLIDARYSEARCHFREQ_PUNCTUAL = 1;
const SOLIDARYSEARCHFREQ_REGULAR = 2;

export const solidarySearchFrequencyLabels = {
  [SOLIDARYSEARCHFREQ_PUNCTUAL]: 'custom.solidary_search.frequency.punctual',
  [SOLIDARYSEARCHFREQ_REGULAR]: 'custom.solidary_search.frequency.regular',
};
