const validationChoices = [
  { id: 0, name: 'Validation automatique' },
  { id: 1, name: 'Validation manuelle' },
  { id: 2, name: 'Validation par le domaine' },
];

const statusChoices = [
  { id: 0, name: 'En attente' },
  { id: 1, name: 'Membre' },
  { id: 2, name: 'Modérateur' },
  { id: 3, name: 'Refusé' },
];

export { validationChoices, statusChoices };
