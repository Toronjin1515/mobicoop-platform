import SolidaryStructureProof from '@material-ui/icons/CheckBox';

export default {
  options: {
    label: 'Eligibilité',
  },
  icon: SolidaryStructureProof,
};
