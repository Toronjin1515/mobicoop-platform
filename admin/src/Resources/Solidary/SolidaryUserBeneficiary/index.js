import SolidaryUserBeneficiaryIcon from '@material-ui/icons/Accessibility';

import { SolidaryUserVolunteerList } from './SolidaryUserBeneficiaryList';

export default {
  options: {
    label: 'Demandeurs solidaires',
  },
  list: SolidaryUserVolunteerList,
  icon: SolidaryUserBeneficiaryIcon,
};
