import SolidaryStructureIcon from '@material-ui/icons/HomeWork';
import SolidaryAnimationList from './SolidaryAnimationList';
import SolidaryAnimationCreate from './SolidaryAnimationCreate';

export default {
  options: {
    label: 'Actions solidaires',
  },
  // list: SolidaryAnimationList,
  create: SolidaryAnimationCreate,
  icon: SolidaryStructureIcon,
};
