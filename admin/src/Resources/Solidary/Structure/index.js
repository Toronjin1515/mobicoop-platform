import SolidaryStructureIcon from '@material-ui/icons/HomeWork';

export default {
  options: {
    label: 'Structure',
  },
  icon: SolidaryStructureIcon,
};
