import PeopleIcon from '@material-ui/icons/People';

import { CommunityUserCreate } from './CommunityUserCreate';
import { CommunityUserEdit } from './CommunityUserEdit';

export default {
  create: CommunityUserCreate,
  edit: CommunityUserEdit,
  icon: PeopleIcon,
};
