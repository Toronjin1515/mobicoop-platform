import EmailIcon from '@material-ui/icons/Email';
import CampaignsList from './CampaignsList';

export default {
  options: {
    label: 'Envois en masse',
  },
  list: CampaignsList,
  icon: EmailIcon,
};
