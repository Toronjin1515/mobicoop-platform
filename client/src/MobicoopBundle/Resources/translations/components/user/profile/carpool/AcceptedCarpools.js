export default {
  fr: {
    carpools: {
      ongoing: "Covoiturages à venir",
      archived: "Covoiturages passés"
    }
  }
}