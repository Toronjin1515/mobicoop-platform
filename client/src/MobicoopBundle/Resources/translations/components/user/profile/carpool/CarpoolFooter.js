export default {
  fr: {
    ui: {
      button: {
        cancel: "Annuler"
      }
    },
    seat: {
      booked: 'place réservée | places réservées'
    },
    passengers: {
      show: "voir les passagers",
      hide: "masquer les passagers"
    },
    driver: {
      show: "voir le conducteur",
      hide: "masquer le conducteur"
    }
  }
}